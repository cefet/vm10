var _MODELOS = [
	{ id: 1, nome: 'Gol', marca_id:1 },
	{ id: 2, nome: 'Golf', marca_id:1 },
	{ id: 3, nome: 'Palio', marca_id:2 },
	{ id: 4, nome: 'Punto', marca_id:2 },
	{ id: 5, nome: 'Chevette', marca_id:3 },
	{ id: 6, nome: 'Camaro', marca_id:3 },
	{ id: 7, nome: 'Focus', marca_id:4 },
	{ id: 8, nome: 'Fusion', marca_id:4 }
];

Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});

$(document).ready(function(){


	var registrarEntrada = function registrarEntrada() {
		try {
			var entrada = {
				veiculo: {
					placa: $('#placa').val(),
					modelo: $('#modelo').data('value'),
					marca: $('#marca').data('value'),
				},
				dataHoraEntrada: $('#data_hora').val()
			};

			var done = function done(response) {
				alert('Entrada do veículo registrada com sucesso');
				window.location.href = 'index.html';
			};
			var fail = function fail(response) {
				alert('Erro ao registrar entrada do veículo');
				console.error(response);
			};

			var options = {
				url: '',
				method: 'POST',
				data: entrada,
				dataType:'JSON'
			};

			$.ajax(options).done(done).fail(fail);
			
		} catch(e) {
			alert('Erro ao registrar entrada');
			console.error(e);
		}
	};

	var listarModelos = function listarModelos(modelos) {
		var html = '';

		if(modelos.length > 0) {
			$(modelos).each(function(index, modelo) {
				html += '<option value="' + modelo.id + '" data-valu="' + modelo.nome + '">' + modelo.nome + '</option>';
			});
		}

		$('#modelo').html(html).attr('disabled', false);
		$('#placa').attr('disabled', false);
		
		$('#data_hora').val(new Date().toLocaleString());
	};

	var carregarModelos = function carregarModelos() {
		var modelos = _MODELOS.filter(function(modelo) {
			return modelo.marca_id == $('#marca').val();
		});

		console.log(modelos)
		listarModelos(modelos);
	};

	$('#form_registrar_entrada').submit(registrarEntrada);

	$('#marca').on('change', carregarModelos);


});