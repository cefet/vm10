package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Veiculo;

public class ColecaoVeiculo {
	
	private final Connection conexao;
	
	public ColecaoVeiculo() throws SQLException{
		conexao = ConnectionFactory.getConnection();
	}
	
	public void adicionar(Veiculo veiculo) throws ColecaoException{
		String sql = "insert into veiculos (placa, modelo, marca) values (?, ?, ?)";
		
		try{
			
			PreparedStatement ps = conexao.prepareStatement(sql);
			
			ps.setString(1, veiculo.getPlaca());
			ps.setString(2, veiculo.getModelo());
			ps.setString(3, veiculo.getMarca());
			
			ps.execute();			
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public Veiculo buscar(String placa) throws ColecaoException{
		String sql = "select * from veiculo where placa = ?";
		Veiculo veiculo = null;
		
		try{
			
			PreparedStatement ps = conexao.prepareStatement(sql);
			
			ps.setString(1, placa);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				veiculo = new Veiculo();
				veiculo.setPlaca(rs.getString("placa"));
				veiculo.setModelo(rs.getString("modelo"));
				veiculo.setMarca(rs.getString("marca"));
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return veiculo;
	}
}
