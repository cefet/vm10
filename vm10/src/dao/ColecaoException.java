package dao;

public class ColecaoException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public ColecaoException(){	
		super();
	}
	
	public ColecaoException(String arg0, Throwable arg1) {
		super(arg0, arg1);	
	}	
}
