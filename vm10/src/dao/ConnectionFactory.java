package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	private static Connection conexao;
	private static final String url = "jdbc:mysql://127.0.0.1/acme";
	
	public static Connection getConnection() throws SQLException{
		if (conexao == null){
			conexao = DriverManager.getConnection(url, "root", "");
		}
		return conexao;
	}
	
}
