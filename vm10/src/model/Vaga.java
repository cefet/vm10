package model;

public class Vaga {
	private TipoVaga tipoVaga;
	private TamanhoVaga tamanhoVaga;
	private int numero;
	public TipoVaga getTipoVaga() {
		return tipoVaga;
	}
	public void setTipoVaga(TipoVaga tipoVaga) {
		this.tipoVaga = tipoVaga;
	}
	public TamanhoVaga getTamanhoVaga() {
		return tamanhoVaga;
	}
	public void setTamanhoVaga(TamanhoVaga tamanhoVaga) {
		this.tamanhoVaga = tamanhoVaga;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}	
}
